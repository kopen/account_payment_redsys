# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import payment
from . import routes

__all__ = ['register', 'routes']


def register():
    Pool.register(
        payment.Journal,
        payment.Payment,
        module='account_payment_redsys', type_='model')
    Pool.register(
        payment.ProcessPayment,
        module='account_payment_redsys', type_='wizard')
    Pool.register(
        payment.RedsysCheckout,
        payment.RedsysResponse,
        module='account_payment_redsys', type_='report')
