# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from urllib.parse import urljoin, quote

from trytond.config import config
from trytond.model import fields
from trytond.report import Report
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.url import http_host

from .redsys import Client

URL_BASE = config.get(
    'account_payment_redsys', 'url_base', default=http_host())


class Journal(metaclass=PoolMeta):
    __name__ = 'account.payment.journal'

    _states = {
        'required': Eval('process_method') == 'redsys',
        'invisible': Eval('process_method') != 'redsys',
    }
    _depends = ['process_method']

    redsys_mode = fields.Selection([
        (None, ""),
        ('live', "Live"),
        ('sandbox', "Sandbox"),
        ], "Mode", states=_states, depends=_depends)
    redsys_merchant_name = fields.Char(
        "Merchant Name", states=_states, depends=_depends)
    redsys_merchant_code = fields.Char(
        "Merchant Code", states=_states, depends=_depends)
    redsys_secret_key = fields.Char(
        "Secret Key", states=_states, depends=_depends)
    redsys_terminal = fields.Integer(
        "Terminal", states=_states, depends=_depends)
    redsys_transaction_type = fields.Integer(
        "Transaction Type", states=_states, depends=_depends)

    del _states, _depends

    @classmethod
    def __setup__(cls):
        super().__setup__()
        redsys_method = ('redsys', "Redsys")
        if redsys_method not in cls.process_method.selection:
            cls.process_method.selection.append(redsys_method)

    @classmethod
    def default_redsys_terminal(cls):
        return 1

    @classmethod
    def default_redsys_transaction_type(cls):
        return 0


class Payment(metaclass=PoolMeta):
    __name__ = 'account.payment'

    redsys_payment_url = fields.Function(
        fields.Char("Redsys Payment URL",
            states={
                'invisible': Eval('state') != 'processing',
                },
            depends=['state']),
        'get_redsys_payment_url')
    redsys_authorization_code = fields.Char(
        "Redsys Authorizaton Code", readonly=True,
        states={
            'invisible': ~Eval('redsys_authorization_code'),
            })
    redsys_response_code = fields.Char(
        "Redsys Response Code", readonly=True,
        states={
            'invisible': ~Eval('redsys_response_code'),
            })

    @classmethod
    def get_redsys_payment_url(cls, payments, name):
        urls = {}
        url_parts = {
            'database': Transaction().database.name,
            }
        for payment in payments:
            if payment.process_method != 'redsys' or not payment.group:
                urls[payment.id] = None
            else:
                url_parts['id'] = payment.id
                urls[payment.id] = urljoin(
                    URL_BASE,
                    quote('/%(database)s/account_payment_redsys/pay/'
                        '%(id)s' % url_parts))
        return urls

    @classmethod
    def process_redsys_response(cls, parameters, signature=None):
        pool = Pool()
        Journal = pool.get('account.payment.journal')
        merchant_parameters = Client.decode_parameters(parameters)
        journals = Journal.search([
                ('process_method', '=', 'redsys'),
                ('redsys_merchant_code', '=',
                    merchant_parameters.get('Ds_MerchantCode')),
                ], limit=1)
        if not journals:
            return False
        journal, = journals

        redsyspayment = Client(
            journal.redsys_merchant_code, journal.redsys_secret_key,
            sandbox=(journal.redsys_mode == 'sandbox'))

        if signature:
            valid_signature = redsyspayment.redsys_check_response(
                signature, parameters)
            if not valid_signature:
                return False

        reference = merchant_parameters.get('Ds_Order')
        authorisation_code = merchant_parameters.get('Ds_AuthorisationCode')
        amount = merchant_parameters.get('Ds_Amount', 0)

        payments = None
        # Search with removing starting zeros because we may have less digits
        # than sent (redsys adds left padding)
        for reference in [reference, reference.lstrip('0')]:
            payments = cls.search([
                ('group.number', 'like', reference + '%%'),
                ('journal.process_method', '=', 'redsys'),
                ('state', 'in', ['processing', 'succeeded', 'failed']),
                ])
            if payments:
                break
        if not payments:
            return []

        response = merchant_parameters.get('Ds_Response')
        for payment in payments:
            payment.redsys_authorization_code = authorisation_code
            payment.redsys_response_code = response
            payment.amount = Decimal(amount) / 100
        cls.save(payments)

        # Process transaction 0000 - 0099: Done
        if int(response) < 100:
            cls.succeed(payments)
        else:
            cls.fail(payments)
        return payments

    def build_redsys_form(self, options=None):
        if options is None:
            options = {}

        url_parts = {
            'database': Transaction().database.name,
            }
        if 'url_ipn' not in options:
            options['url_ipn'] = urljoin(
                    URL_BASE,
                    quote('/%(database)s/account_payment_redsys/webkook' %
                        url_parts))
        if 'url_confirm' not in options:
            options['url_confirm'] = urljoin(
                    URL_BASE,
                    quote('/%(database)s/account_payment_redsys/response' %
                        url_parts))
        if 'url_cancel' not in options:
            options['url_cancel'] = options['url_confirm']

        redsys_reference = self.group.number.rjust(4, '0')[:12]

        merchant_code = self.journal.redsys_merchant_code
        merchant_secret_key = self.journal.redsys_secret_key

        if 'description' not in options:
            if self.origin:
                options['description'] = self.origin.rec_name
            else:
                options['description'] = (
                    self.description or str(self.id))

        values = {
            'DS_MERCHANT_AMOUNT': self.amount,
            'DS_MERCHANT_CURRENCY': (
                self.journal.currency.numeric_code),
            'DS_MERCHANT_ORDER': redsys_reference,
            'DS_MERCHANT_PRODUCTDESCRIPTION': options['description'],
            'DS_MERCHANT_TITULAR': self.journal.redsys_merchant_name,
            'DS_MERCHANT_MERCHANTCODE': merchant_code,
            'DS_MERCHANT_MERCHANTURL': options['url_ipn'],
            'DS_MERCHANT_URLOK': options['url_confirm'],
            'DS_MERCHANT_URLKO': options['url_cancel'],
            'DS_MERCHANT_MERCHANTNAME': self.journal.redsys_merchant_name,
            'DS_MERCHANT_TERMINAL': self.journal.redsys_terminal,
            'DS_MERCHANT_TRANSACTIONTYPE': (
                self.journal.redsys_transaction_type),
            }
        redsyspayment = Client(
            merchant_code, merchant_secret_key,
            sandbox=(self.journal.redsys_mode == 'sandbox'))
        return redsyspayment.redsys_generate_request(values)


class ProcessPayment(metaclass=PoolMeta):
    __name__ = 'account.payment.process'

    def _group_payment_key(self, payment):
        key = super()._group_payment_key(payment)
        # Ensure unique payment is included on each group
        if payment.journal.process_method == 'redsys':
            key = key + (('redsys_id', payment.id),)
        return key

    def _new_group(self, values):
        if 'redsys_id' in values:
            values = values.copy()
            values.pop('redsys_id')
        return super()._new_group(values)


class RedsysCheckout(Report):
    "Redsys Checkout"
    __name__ = 'account.payment.redsys.checkout'

    @classmethod
    def get_context(cls, records, data):
        context = super().get_context(records, data)
        context['payment'] = context['record']
        context['form'] = context['payment'].build_redsys_form()
        return context


class RedsysResponse(Report):
    "Redsys Response"
    __name__ = 'account.payment.redsys.response'

    @classmethod
    def get_context(cls, records, data):
        context = super().get_context(records, data)
        context['payment'] = context['record']
        return context
