Account Payment Redsys Module
#############################

The account_payment_redsys module allows to process payments using
`Redsys <http://www.redsys.es/>` payment gateway. Redsys is an Spanish Gateway
used to process payment card payments

It setups payment via a redirect option. So in order to perform the payment
the user should open a link on it's browser where she inputs the card details.

Once the payment is process by the gateway the user is redirected back to
Tryton.
