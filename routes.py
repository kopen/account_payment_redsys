# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import logging
import http.client

from werkzeug.exceptions import abort
from werkzeug.wrappers import Response

from trytond.wsgi import app
from trytond.protocols.wrappers import with_pool, with_transaction

logger = logging.getLogger(__name__)


@app.route(
    '/<database_name>/account_payment_redsys/pay/<id>',
    methods=['GET'])
@with_pool
@with_transaction()
def checkout(request, pool, id):
    Payment = pool.get('account.payment')
    try:
        payment, = Payment.search([
                ('id', '=', id),
                ])
    except ValueError:
        abort(403)
    Report = pool.get('account.payment.redsys.checkout', type='report')
    # TODO language
    ext, content, _, _ = Report.execute([payment.id], {})
    assert ext == 'html'
    return Response(content, 200, content_type='text/html')


@app.route(
    '/<database_name>/account_payment_redsys/response',
    methods=['GET'])
@with_pool
@with_transaction(readonly=False)
def response(request, pool):
    Payment = pool.get('account.payment')
    Report = pool.get('account.payment.redsys.response', type='report')
    # TODO: Get parameters
    parameters = request.args.get('Ds_MerchantParameters')
    if not parameters:
        abort(400)
    payments = Payment.process_redsys_response(
        parameters,
        request.args.get('Ds_Signature'))
    ext, content, _, _ = Report.execute([p.id for p in payments], {})
    assert ext == 'html'
    return Response(content, 200, content_type='text/html')


@app.route(
    '/<database_name>/account_payment_redsys/webhook',
    methods={'POST'})
@with_pool
@with_transaction()
def webhooks_endpoint(request, pool):
    Payment = pool.get('account.payment')
    payments = Payment.process_redsys_response()
    if payments is False:
        return Response(status=http.client.BAD_REQUEST)
    elif not payments:
        return Response(status=http.client.NOT_FOUND)
    return Response(status=http.client.NO_CONTENT)
