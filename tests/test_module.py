# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from decimal import Decimal

from trytond.pool import Pool
from trytond.transaction import Transaction

from trytond.modules.currency.tests import create_currency
from trytond.modules.company.tests import create_company, set_company
from trytond.modules.account.tests import create_chart


def setup_journal(company):
    pool = Pool()
    Journal = pool.get('account.payment.journal')
    journal = Journal()
    journal.name = 'Redsys'
    journal.company = company
    journal.currency = company.currency
    journal.process_method = 'redsys'
    journal.redsys_mode = 'sandbox'
    journal.redsys_merchant_name = 'Tryton'
    journal.redsys_merchant_code = '999008881'
    journal.redsys_secret_key = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'
    journal.save()
    return journal


class AccountPaymentRedsysTestCase(ModuleTestCase):
    'Test Account Payment Redsys module'
    module = 'account_payment_redsys'

    @with_transaction()
    def test_redsys_workflow(self):
        'Test redsys workflow is correctly generated'
        pool = Pool()
        Party = pool.get('party.party')
        Payment = pool.get('account.payment')
        Date = pool.get('ir.date')
        ProcessPayment = pool.get('account.payment.process', type='wizard')

        currency = create_currency('EUR')
        company = create_company(currency=currency)
        with set_company(company):
            create_chart(company)
            customer = Party(name='Customer')
            customer.save()
            journal = setup_journal(company)

            payment, = Payment.create([{
                        'company': company,
                        'party': customer,
                        'journal': journal,
                        'kind': 'receivable',
                        'amount': Decimal('1000.0'),
                        'state': 'submitted',
                        'description': 'PAYMENT',
                        'date': Date.today(),
                        }])
            session_id, _, _ = ProcessPayment.create()
            process_payment = ProcessPayment(session_id)
            with Transaction().set_context(
                    active_model=Payment.__name__,
                    active_ids=[payment.id]):
                _, data = process_payment.do_process(None)

            parameters = payment.build_redsys_form(options={
                    'url_ipn': 'redsys_ipn',
                    'url_confirm': 'redsys_response',
                    })

            self.assertTrue('DS_Redsys_Url' in parameters)
            self.assertEqual(
                parameters['DS_SignatureVersion'], 'HMAC_SHA256_V1')
            self.assertTrue('DS_MerchantParameters' in parameters)
            self.assertTrue('DS_Signature' in parameters)

            Payment.process_redsys_response(
                'eyJEc19EYXRlIjoiMjclMkYwNSUyRjIwMjAiLCJEc19Ib3VyIjoiMTIlM0E1'
                'MCIsIkRzX1NlY3VyZVBheW1lbnQiOiIxIiwiRHNfQW1vdW50IjoiMTAwMDAw'
                'IiwiRHNfQ3VycmVuY3kiOiI5NzgiLCJEc19PcmRlciI6IjAwMDAwMDAwMDEi'
                'LCJEc19NZXJjaGFudENvZGUiOiI5OTkwMDg4ODEiLCJEc19UZXJtaW5hbCI6'
                'IjAwMSIsIkRzX1Jlc3BvbnNlIjoiMDAwMCIsIkRzX1RyYW5zYWN0aW9uVHlw'
                'ZSI6IjAiLCJEc19NZXJjaGFudERhdGEiOiIiLCJEc19BdXRob3Jpc2F0aW9u'
                'Q29kZSI6IjIyMjE1MyIsIkRzX0NhcmRfTnVtYmVyIjoiNDU0ODgxKioqKioq'
                'MDAwNCIsIkRzX0NvbnN1bWVyTGFuZ3VhZ2UiOiIxIiwiRHNfQ2FyZF9Db3Vu'
                'dHJ5IjoiNzI0IiwiRHNfQ2FyZF9CcmFuZCI6IjEifQ==',
                'Ao8MhZADPhXkqHSGEjc6AFV9ucIpMyy45Nthp9_wwDg=')

            payment = Payment(payment.id)
            self.assertEqual(payment.state, 'succeeded')
            self.assertEqual(payment.redsys_response_code, '0000')
            self.assertEqual(payment.redsys_authorization_code, '222153')
del ModuleTestCase
