===============================
Account Payment Redsys Scenario
===============================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts

Install account_payment_redsys::

    >>> config = activate_modules('account_payment_redsys')

Create company::

    >>> Company = Model.get('company.company')
    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)

Create payment journal::

    >>> PaymentJournal = Model.get('account.payment.journal')
    >>> payment_journal = PaymentJournal(name="Redsys")
    >>> payment_journal.process_method = 'redsys'
    >>> payment_journal.redsys_mode = 'sandbox'
    >>> payment_journal.redsys_merchant_name = 'Tryton'
    >>> payment_journal.redsys_merchant_code = '999008881'
    >>> payment_journal.redsys_secret_key = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'
    >>> payment_journal.save()

Create party::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create two approved payment::

    >>> Payment = Model.get('account.payment')
    >>> payment = Payment()
    >>> payment.journal = payment_journal
    >>> payment.kind = 'receivable'
    >>> payment.party = customer
    >>> payment.amount = Decimal('42')
    >>> payment.description = 'Testing'
    >>> payment.click('submit')
    >>> payment.state
    'submitted'
    >>> other_payment, = payment.duplicate()
    >>> other_payment.click('submit')
    >>> other_payment.state
    'submitted'

Processing multiple payments generates one group for each::

    >>> Group = Model.get('account.payment.group')
    >>> process_payment = Wizard('account.payment.process',
    ...     [payment, other_payment])
    >>> process_payment.execute('process')
    >>> payment.state
    'processing'
    >>> other_payment.state
    'processing'
    >>> len(Group.find([]))
    2
